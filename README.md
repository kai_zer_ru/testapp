# TestApp

## Зависимости
- docker
- composer

## Подготовка
Создать файл `.env` 

```.dotenv
APP_NAME=testApp
APP_ENV=local
APP_KEY=base64:mlglJK+oAOa09rHO1uZ4J2Im/N0R/tqzimaq/uts+sY=
APP_DEBUG=true
APP_URL=http://testApp.test

LOG_CHANNEL=stack
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=testapp
DB_USERNAME=root
DB_PASSWORD=

REDIS_HOST=redis
REDIS_PASSWORD=null
REDIS_PORT=6379
```

## Запуск

```shell
composer install
docker-compose up --build -d
```

## Использование

В браузере открыть адрес [http://127.0.0.1/api/getOrgByInn?inn=123&type=a](`http://127.0.0.1/api/getOrgByInn?inn=123&type=a`)

Параметры:

- type - тип реализации получения данных из задания
- inn - вводимые данные (ИНН)

В ответе будет JSON
`{"source": 2,"kpp":123456,"name":"Organization Name","ешьуЭЖ0ю00011491775512695312, "error": ""}` где `source` - ИД источника, `kpp` - КПП искомой организации, `name` - название искомой организации, `error` - ошибка

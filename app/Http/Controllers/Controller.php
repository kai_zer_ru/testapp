<?php

namespace App\Http\Controllers;

use App\Interfaces\SourceInterface;
use App\Models\SourceOne;
use App\Models\SourceThree;
use App\Models\SourceTwo;
use BeyondCode\ServerTiming\Facades\ServerTiming;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    private Request $request;
    
    public function __construct(Request $request)
    {
    	$this->request = $request;
    }
	
	public function getOrgByInn()
	{
		ServerTiming::start('Running getOrgByInn');
		
		$type = $this->request->input('type', 'a');
		$inn = $this->request->input('inn', '');
		if (!$inn) {
			ServerTiming::stop('Running getOrgByInn');
			return response()->json([], 404);
		}
		$mainResponse = [];
		
		/**
		 * @var SourceInterface $sourceClass
		 */
		$sourcesAll = [
			SourceOne::class,
			SourceTwo::class,
			SourceThree::class,
		];
		switch ($type) {
			case 'a':
				foreach ($sourcesAll as $source) {
					Log::info('Start source '.$source);
					$sourceClass = new $source();
					$response = $sourceClass->getOrgByInn($inn);
					if (is_bool($response)) {
						Log::info('response = false');
					} else {
						Log::info('response = '.json_encode($response));
					}
					if ($sourceClass->incrementStat($response, $source)) {
						$mainResponse = $response;
						break;
					}
				}
				break;
			case 'b':
//				$unCount = [];
				$sources = Redis::connection()->zRangeByScore('timers.'.date('Y-m-d'), 0, 10);
				Log::info(json_encode($sources));
				foreach ($sources as $source) {
					Log::info('Start source '.$source);
					$counter = Redis::connection()->hGet('counter', $source);
					Log::info('Source '.$source.' counter = '.$counter);
					if ($counter < 10) {
//						$unCount []= $source;
						continue;
					}
					$sourceClass = new $source();
					$response = $sourceClass->getOrgByInn($inn);
					if (is_bool($response)) {
						Log::info('response = false');
					} else {
						Log::info('response = '.json_encode($response));
					}
					if ($sourceClass->incrementStat($response, $source)) {
						$mainResponse = $response;
						break;
					}
				}
				break;
			case 'c':
				foreach ($sourcesAll as $source) {
					Log::info('Start source '.$source);
					$isBan = Redis::connection()->hGet('ban.'.date('Y-m-d H'), $source);
					Log::info('Source '.$source.' ban = '.$isBan);
					if ($isBan) {
						continue;
					}
					$errorCount = Redis::connection()->hGet('errorsHour.'.date('Y-m-d H'), $source);
					Log::info('Source '.$source.' errorCount = '.$errorCount);
					if ($errorCount > 5) {
						Redis::connection()->hSet('ban.'.date('Y-m-d H'), $source, 1);
						Redis::connection()->expire('ban.'.date('Y-m-d H'), 60*60);
						continue;
					}
					$sourceClass = new $source();
					$response = $sourceClass->getOrgByInn($inn);
					if (is_bool($response)) {
						Log::info('response = false');
					} else {
						Log::info('response = '.json_encode($response));
					}
					if ($sourceClass->incrementStat($response, $source)) {
						$mainResponse = $response;
						break;
					}
				}
				break;
			case 'd':
				// параллельная работа
				break;
			default:
				ServerTiming::stop('Running getOrgByInn');
				$mainResponse['error'] = 'Wrong type';
				return response()->json($mainResponse);
		}
		$mainResponse['error'] = '';
		if (!array_key_exists('source', $mainResponse)) {
			$mainResponse['error'] = 'No data';
		}
		unset($mainResponse['time']);
		ServerTiming::stop('Running getOrgByInn');
    	return response()->json($mainResponse);
    }
}

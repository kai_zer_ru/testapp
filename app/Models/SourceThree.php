<?php
	namespace App\Models;
	
	use App\Interfaces\SourceInterface;
	
	class SourceThree extends Source implements SourceInterface {
		/**
		 * @param string $inn
		 * @return array|bool
		 */
		public function getOrgByInn(string $inn) {
			$st = microtime(true);
			$r = rand(0, 10);
			if ($r > 7) { // эмуляция ошибки
				return false;
			}
			$sleep = rand(0, 1000)/1000;
			sleep($sleep);
			$time = microtime(true)-$st;
			return ['source' => 3, 'kpp' => 123456, 'name' => 'Organization Name', 'time' => $time];
		}
	}

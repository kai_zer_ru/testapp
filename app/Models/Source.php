<?php

	namespace App\Models;
	use Illuminate\Database\Eloquent\Model;
	use Illuminate\Support\Arr;
	use Illuminate\Support\Facades\Redis;
	
	class Source extends Model {
		/**
		 * @param array|bool $response
		 * @param $source
		 * @return bool
		 */
		public function incrementStat($response, $source) : bool{
			Redis::connection()->hIncrBy('counter', $source, 1);
			if (false === $response) {
				// инкрементим счётчик ошибок
				Redis::connection()->hIncrBy('errors', $source, 1);
				Redis::connection()->hIncrBy('errorsHour.'.date('Y-m-d H'), $source, 1);
				Redis::connection()->hIncrBy('errorsDate.'.date('Y-m-d'), $source, 1);
				return false;
			}
			$time = Arr::get($response, 'time', 0);
			$timeOld = Redis::connection()->zScore('timers.'.date('Y-m-d'), $source);
			if (!$timeOld || $timeOld > $time) {
				Redis::connection()->zAdd('timers.' . date('Y-m-d'), $time, $source);
			}
			if ($time > 1) {
				return false;
			}
			return true;
		}
	}

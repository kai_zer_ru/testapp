<?php
	namespace App\Models;
	use App\Interfaces\SourceInterface;
	
	class SourceOne extends Source implements SourceInterface {
		/**
		 * @param string $inn
		 * @return array|bool
		 */
		public function getOrgByInn(string $inn) {
			$st = microtime(true);
			$r = rand(0, 10);
			if ($r > 2) { // эмуляция ошибки
				return false;
			}
			$sleep = rand(0, 4000)/1000; // задержка от 0 до 4 секунд, эмулируем тормоза в сервисе
			sleep($sleep);
			$time = microtime(true)-$st;
			return ['source' => 1, 'kpp' => 123456, 'name' => 'Organization Name', 'time' => $time];
		}
	}
